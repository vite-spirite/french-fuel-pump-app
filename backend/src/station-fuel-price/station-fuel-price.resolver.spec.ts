import { Test, TestingModule } from '@nestjs/testing';
import { StationFuelPriceResolver } from './station-fuel-price.resolver';
import { StationFuelPriceService } from './station-fuel-price.service';

describe('StationFuelPriceResolver', () => {
  let resolver: StationFuelPriceResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StationFuelPriceResolver, StationFuelPriceService],
    }).compile();

    resolver = module.get<StationFuelPriceResolver>(StationFuelPriceResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
