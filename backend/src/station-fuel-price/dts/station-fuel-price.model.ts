import { Field, ObjectType } from '@nestjs/graphql';
import { FuelType } from 'src/fuel-type/dts/fuel-type.model';
import { Station } from 'src/stations/dts/station.model';
import {Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn} from 'typeorm';

@Entity({name: 'station_fuel_prices'})
@ObjectType()
export class stationFuelPrice {
    @PrimaryGeneratedColumn()
    id: number

    @OneToOne(() => FuelType, ft => ft.id, {createForeignKeyConstraints: false})
    @JoinColumn()
    @Field(type => FuelType)
    fuel: FuelType

    @OneToOne(() => Station, s => s.id, {createForeignKeyConstraints: false})
    @JoinColumn()
    @Field(type => Station)
    station: Station

    @Column()
    @Field()
    is_distribute: boolean

    @Column()
    @Field()
    price: number
    
    @Column()
    @Field()
    update: Date
}