import { Module } from '@nestjs/common';
import { StationFuelPriceService } from './station-fuel-price.service';
import { StationFuelPriceResolver } from './station-fuel-price.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { stationFuelPrice } from './dts/station-fuel-price.model';

@Module({
  imports: [TypeOrmModule.forFeature([stationFuelPrice])],
  providers: [StationFuelPriceResolver, StationFuelPriceService]
})
export class StationFuelPriceModule {}
