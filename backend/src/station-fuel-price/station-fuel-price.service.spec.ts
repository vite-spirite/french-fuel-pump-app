import { Test, TestingModule } from '@nestjs/testing';
import { StationFuelPriceService } from './station-fuel-price.service';

describe('StationFuelPriceService', () => {
  let service: StationFuelPriceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StationFuelPriceService],
    }).compile();

    service = module.get<StationFuelPriceService>(StationFuelPriceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
