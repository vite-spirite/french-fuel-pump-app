import { Resolver } from '@nestjs/graphql';
import { StationFuelPriceService } from './station-fuel-price.service';

@Resolver()
export class StationFuelPriceResolver {
  constructor(private readonly stationFuelPriceService: StationFuelPriceService) {}
}
