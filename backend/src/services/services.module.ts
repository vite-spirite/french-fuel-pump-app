import { Module } from '@nestjs/common';
import { ServicesService } from './services.service';
import { ServicesResolver } from './services.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from './dts/service.model';

@Module({
  imports: [
    TypeOrmModule.forFeature([Service]),
  ],
  providers: [ServicesResolver, ServicesService],
  exports: [ServicesService]
})
export class ServicesModule {}
