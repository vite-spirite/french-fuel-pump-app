import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Service } from './dts/service.model';

@Injectable()
export class ServicesService {
    constructor(@InjectRepository(Service) private seviceRepository: Repository<Service>) {} 

    async get(): Promise<Service[]> {
        return await this.seviceRepository.find();
    }

    async findOrCreate(name: string): Promise<Service> {
        const service = await this.seviceRepository.findOne({where: {name}});
        if(service) {
            return service;
        }

        const _service = await this.seviceRepository.save(this.seviceRepository.create({name}));
        return _service;        
    }

    async save(data: Service[]): Promise<Service[]> {
        return await this.seviceRepository.save(data, {chunk: 200});
    }
}
