import {Field, ID, ObjectType} from '@nestjs/graphql';
import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@ObjectType()
@Entity({name: 'services'})
export class Service {
    @PrimaryGeneratedColumn()
    @Field(types => ID)
    id: number

    @Field()
    @Column()
    name: string
}