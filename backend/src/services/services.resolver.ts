import { Query, Resolver } from '@nestjs/graphql';
import { Service } from './dts/service.model';
import { ServicesService } from './services.service';

@Resolver()
export class ServicesResolver {
  constructor(private readonly servicesService: ServicesService) {}

  @Query(returns => [Service])
  async getServices(): Promise<Service[]> {
    return await this.servicesService.get();
  }
}
