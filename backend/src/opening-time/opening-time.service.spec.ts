import { Test, TestingModule } from '@nestjs/testing';
import { OpeningTimeService } from './opening-time.service';

describe('OpeningTimeService', () => {
  let service: OpeningTimeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OpeningTimeService],
    }).compile();

    service = module.get<OpeningTimeService>(OpeningTimeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
