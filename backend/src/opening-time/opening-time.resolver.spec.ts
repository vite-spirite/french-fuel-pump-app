import { Test, TestingModule } from '@nestjs/testing';
import { OpeningTimeResolver } from './opening-time.resolver';
import { OpeningTimeService } from './opening-time.service';

describe('OpeningTimeResolver', () => {
  let resolver: OpeningTimeResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OpeningTimeResolver, OpeningTimeService],
    }).compile();

    resolver = module.get<OpeningTimeResolver>(OpeningTimeResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
