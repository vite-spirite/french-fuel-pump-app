import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { OpeningTime } from './dts/opening-time.model';

@Injectable()
export class OpeningTimeService {
    constructor(@InjectRepository(OpeningTime) private readonly openingTimeRepository: Repository<OpeningTime>) {}

    async create(data: DeepPartial<OpeningTime>): Promise<OpeningTime> {
        return await this.openingTimeRepository.save(
            this.openingTimeRepository.create(data),
        );
    }
}
