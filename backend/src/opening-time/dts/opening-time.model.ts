import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Field, ObjectType} from '@nestjs/graphql';
import { Station } from 'src/stations/dts/station.model';

@Entity({name: 'schedules'})
@ObjectType()
export class OpeningTime {
    @PrimaryGeneratedColumn()
    id: number
    
    @OneToOne(() => Station, station => station.id, {createForeignKeyConstraints: false})
    @JoinColumn()
    station: Station

    @Field()
    @Column()
    dayOfWeek: number

    @Field()
    @Column({nullable: true})
    closure?: boolean = null;

    @Field()
    @Column({nullable: true})
    open?: string = null;
    
    @Field()
    @Column({nullable: true})
    close?: string = null;
}
