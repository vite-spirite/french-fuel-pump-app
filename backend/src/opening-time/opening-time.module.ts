import { Module } from '@nestjs/common';
import { OpeningTimeService } from './opening-time.service';
import { OpeningTimeResolver } from './opening-time.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OpeningTime } from './dts/opening-time.model';

@Module({
  imports: [TypeOrmModule.forFeature([OpeningTime])],
  providers: [OpeningTimeResolver, OpeningTimeService],
  exports: [OpeningTimeService],
})
export class OpeningTimeModule {}
