import { Resolver } from '@nestjs/graphql';
import { OpeningTimeService } from './opening-time.service';

@Resolver()
export class OpeningTimeResolver {
  constructor(private readonly openingTimeService: OpeningTimeService) {}
}
