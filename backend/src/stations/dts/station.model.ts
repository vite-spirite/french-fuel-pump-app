import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import {ObjectType, Field, ID} from '@nestjs/graphql';
import { nanoid } from "nanoid";
import { City } from "src/cities/dts/city.model";
import { Service } from "src/services/dts/service.model";
import { OpeningTime } from "src/opening-time/dts/opening-time.model";
import { stationFuelPrice } from "src/station-fuel-price/dts/station-fuel-price.model";

@Entity({name: "stations"})
@ObjectType()
export class Station {
    @PrimaryGeneratedColumn()
    id: number

    @Column({unique: true})
    @Field(type => ID)
    gouv_id: number

    @Column({type: "float"})
    @Field()
    lat: number

    @Column({type: "float"})
    @Field()
    lng: number

    @OneToOne(() => City, city => city.id, {createForeignKeyConstraints: false, cascade: true})
    @JoinColumn()
    @Field(type => City)
    city: City

    @ManyToMany(() => Service, {cascade: true})
    @JoinTable()
    @Field(type => [Service], {nullable: true})
    services?: Service[]|null = null

    @Field(type => [OpeningTime])
    @OneToMany(() => OpeningTime, ot => ot.station, {cascade: true})
    OpeningTimes: OpeningTime[]

    @OneToMany(() => stationFuelPrice, sfp => sfp.station)
    @Field(type => [stationFuelPrice], {nullable: true})
    prices: stationFuelPrice[]

    @Column()
    @Field()
    address: string

    @Column()
    @Field()
    automaton: boolean

    @Column({default: false})
    @Field({nullable: true})
    closure?: boolean = false

    @Column({default: null})
    @Field({nullable: true})
    closure_type?: boolean = null //0 temp | 1 def

    @Column({default: null})
    @Field({nullable: true})
    closure_temp_start?: Date = null

    @Column({default: null})
    @Field({nullable: true})
    closure_temp_end?: Date = null
    
    @Field({nullable: true})
    distance?: number
}