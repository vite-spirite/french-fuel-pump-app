import { Field, ObjectType } from "@nestjs/graphql";
import { Station } from "./station.model";

@ObjectType()
export class PaginateStation {
    @Field()
    current: number
    @Field()
    page: number

    @Field(type => [Station], {nullable: true})
    stations: Station[]
}