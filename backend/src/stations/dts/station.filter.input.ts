import { ID, InputType, Field } from "@nestjs/graphql";

@InputType()
export class StationFilterInput {
    @Field(type => [ID], {nullable: true, defaultValue: []})
    fuels: number[]
    
    @Field(type => [ID], {nullable: true, defaultValue: []})
    services: number[]
}