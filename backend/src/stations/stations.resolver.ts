import { Args, Query, Resolver } from '@nestjs/graphql';
import { CoordUserType } from './dts/coords.input';
import { PaginateStation } from './dts/paginate.type';
import { StationFilterInput } from './dts/station.filter.input';
import { Station } from './dts/station.model';
import { StationsService } from './stations.service';

@Resolver()
export class StationsResolver {
  constructor(private readonly stationsService: StationsService) {}

  @Query(returns => PaginateStation)
  async getStations(@Args('coords') coords: CoordUserType, @Args('filters', {nullable: true, defaultValue: {fuels: [], services: []}}) filters: StationFilterInput, @Args('page', {nullable: true, defaultValue: 0}) page: number): Promise<PaginateStation> {
    return await this.stationsService.get(coords, filters, page);
  }
}
