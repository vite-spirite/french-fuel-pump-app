import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { City } from 'src/cities/dts/city.model';
import { FuelType } from 'src/fuel-type/dts/fuel-type.model';
import { Service } from 'src/services/dts/service.model';
import { stationFuelPrice } from 'src/station-fuel-price/dts/station-fuel-price.model';
import { DeepPartial, getConnection, In, QueryBuilder, Repository, SelectQueryBuilder } from 'typeorm';
import { CoordUserType } from './dts/coords.input';
import { PaginateStation } from './dts/paginate.type';
import { StationFilterInput } from './dts/station.filter.input';
import { Station } from './dts/station.model';

@Injectable()
export class StationsService {
    constructor(@InjectRepository(Station) private readonly stationRepository: Repository<Station>) {}

    async getByGouvId(gouvId: number): Promise<Station> {
        const _station = await this.stationRepository.findOne({
            where: {gouv_id: gouvId},
            relations: ["city", "services"]
        });

        return _station;
    }

    async saveOrCreate(data: DeepPartial<Station>[]): Promise<Station[]> {
        let _station = await this.stationRepository.save(data);
        return _station;
    }

    async get(coords: CoordUserType,filter: StationFilterInput, page: number = 0): Promise<PaginateStation> {
        //50.8727776 = user latitude | 2.0859883 = user longitude
        
        const _filtering = await getConnection().query(`
        SELECT s.id, getdistancestation(lat, lng, $1, $2) as distance
        FROM "stations" AS s
        LEFT JOIN "station_fuel_prices" AS sfp ON s.id = sfp."stationId"
        LEFT JOIN "stations_services_services" AS sss ON s.id = sss."stationsId"
        GROUP BY s.id, sfp."stationId", sss."stationsId"
        HAVING array_agg(sfp."fuelId") @> $3 AND array_agg(sss."servicesId") @> $4
        ORDER BY distance ASC LIMIT 25 OFFSET $5`, [coords.lat, coords.lng, filter.fuels, filter.services, page*25]);

        const items = await this.stationRepository.query(`
            SELECT s.id
            FROM "stations" AS s
            LEFT JOIN "station_fuel_prices" AS sfp ON s.id = sfp."stationId"
            LEFT JOIN "stations_services_services" AS sss ON s.id = sss."stationsId"
            GROUP BY s.id, sfp."stationId", sss."stationsId"
            HAVING array_agg(sfp."fuelId") @> $1 AND array_agg(sss."servicesId") @> $2
        `, [filter.fuels, filter.services]);

        const _stations = await this.stationRepository.createQueryBuilder()
            .leftJoinAndSelect('Station.city', 'city')
            .leftJoinAndSelect('Station.services', 'services')
            .leftJoinAndSelect(`Station.prices`, 'prices', 'prices."stationId" = "Station".id')
            .leftJoinAndSelect('prices.fuel', 'fuel')
            .leftJoinAndSelect('Station.OpeningTimes', 'OpeningTimes')
            .orderBy('prices.update', 'DESC')
            .whereInIds(_filtering.map(val => val.id))
            .cache(600000)
            .getMany()

        _stations.map(val => val.distance = (_filtering as any[]).find(el => el.id === val.id).distance);
        _stations.sort((a, b) => a.distance - b.distance);

        const _paginate: PaginateStation = {
            page: Math.floor(items.length/25),
            current: page,
            stations: _stations
        };
        
        return _paginate;
    }
}
