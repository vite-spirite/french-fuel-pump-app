import { Module } from '@nestjs/common';
import { StationsService } from './stations.service';
import { StationsResolver } from './stations.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Station } from './dts/station.model';

@Module({
  imports: [
    TypeOrmModule.forFeature([Station])
  ],
  providers: [StationsResolver, StationsService],
  exports: [StationsService]
})
export class StationsModule {}
