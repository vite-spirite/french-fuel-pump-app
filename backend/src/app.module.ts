import { ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { graphql } from './configuration/graphql';
import { ScheduleModule } from '@nestjs/schedule';
import { CitiesModule } from './cities/cities.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { database } from './configuration/database';
import { Connection } from 'typeorm';
import { ServicesModule } from './services/services.module';
import { StationsModule } from './stations/stations.module';
import { OpeningTimeModule } from './opening-time/opening-time.module';
import { FuelTypeModule } from './fuel-type/fuel-type.module';
import { StationFuelPriceModule } from './station-fuel-price/station-fuel-price.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot(database),
    GraphQLModule.forRoot<ApolloDriverConfig>(graphql),
    CitiesModule,
    ServicesModule,
    StationsModule,
    OpeningTimeModule,
    FuelTypeModule,
    StationFuelPriceModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {
    this.connection.synchronize();
  }
}
