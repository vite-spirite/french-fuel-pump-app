import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CityCreateArgs } from './dts/city.create.dts';
import { City } from './dts/city.model';


@Injectable()
export class CitiesService {
    constructor(@InjectRepository(City) private cityRepository: Repository<City>) {}

    async get(): Promise<City[]> {
        return await this.cityRepository.find();
    }

    async save(data: CityCreateArgs[]): Promise<City[]> {
        return await this.cityRepository.save(data);
    }

    async findOrCreate(data: CityCreateArgs): Promise<City> {
        const city = await this.cityRepository.findOne({where: {code: data.code}});

        if(city) {
            return city;
        }

        let _city = this.cityRepository.create({
            ...data
        });

        _city = await this.cityRepository.save(_city);

        return _city;
    }
} 
