import { Field, ID, ObjectType } from '@nestjs/graphql';
import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@ObjectType()
@Entity({name: 'cities'})
export class City {
    @PrimaryGeneratedColumn()
    @Field(types => ID)
    id: number

    @Field()
    @Column()
    name: string

    @Field()
    @Column()
    code: string
}