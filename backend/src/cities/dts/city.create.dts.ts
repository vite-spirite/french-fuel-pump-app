export interface CityCreateArgs {
    name: string
    code: string
}