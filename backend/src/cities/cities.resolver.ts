import { Query, Resolver } from '@nestjs/graphql';
import { CitiesService } from './cities.service';
import { City } from './dts/city.model';

@Resolver()
export class CitiesResolver {
  constructor(private readonly citiesService: CitiesService) {}

  @Query(returns => [City]) 
  async getCities(): Promise<City[]> {
    return await this.citiesService.get();
  }
}
