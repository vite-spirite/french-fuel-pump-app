import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { env } from "process";

export const database: TypeOrmModuleOptions = {
    type: "postgres",
    username: env.DB_USER,
    password: env.DB_PASSWORD,
    host: env.DB_HOST,
    database: env.DB_NAME,
    autoLoadEntities: true,
    logging: "all",
    maxQueryExecutionTime: 1000,
    cache: true
}