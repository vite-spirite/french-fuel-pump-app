import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { join } from 'path';

export const graphql: ApolloDriverConfig = {
    driver: ApolloDriver,
    playground: true,
    debug: true,
    autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    sortSchema: true,
    context: ({req}) => ({req}),
}