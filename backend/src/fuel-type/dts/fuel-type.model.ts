import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@Entity({name: 'fuel_type'})
@ObjectType()
export class FuelType {
    @PrimaryGeneratedColumn()
    @Field(type => ID)
    id: number

    @Column()
    gouv_id: number

    @Field()
    @Column()
    name: string
}