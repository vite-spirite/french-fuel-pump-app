import { Query, Resolver } from '@nestjs/graphql';
import { FuelType } from './dts/fuel-type.model';
import { FuelTypeService } from './fuel-type.service';

@Resolver()
export class FuelTypeResolver {
  constructor(private readonly fuelTypeService: FuelTypeService) {}

  @Query(returns => [FuelType])
  async getFuelType(): Promise<FuelType[]> {
    return await this.fuelTypeService.get();
  }
}
