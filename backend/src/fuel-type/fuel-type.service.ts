import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FuelType } from './dts/fuel-type.model';

@Injectable()
export class FuelTypeService {
    constructor(@InjectRepository(FuelType) private readonly fuelTypeRepository: Repository<FuelType>) {}

    async get(): Promise<FuelType[]> {
        return await this.fuelTypeRepository.find();
    }
}
