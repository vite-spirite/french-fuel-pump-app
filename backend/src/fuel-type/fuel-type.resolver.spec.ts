import { Test, TestingModule } from '@nestjs/testing';
import { FuelTypeResolver } from './fuel-type.resolver';
import { FuelTypeService } from './fuel-type.service';

describe('FuelTypeResolver', () => {
  let resolver: FuelTypeResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FuelTypeResolver, FuelTypeService],
    }).compile();

    resolver = module.get<FuelTypeResolver>(FuelTypeResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
