import { Module } from '@nestjs/common';
import { FuelTypeService } from './fuel-type.service';
import { FuelTypeResolver } from './fuel-type.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FuelType } from './dts/fuel-type.model';

@Module({
  imports: [TypeOrmModule.forFeature([FuelType])],
  providers: [FuelTypeResolver, FuelTypeService]
})
export class FuelTypeModule {}
