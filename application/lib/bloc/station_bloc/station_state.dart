part of 'station_bloc.dart';

abstract class StationState extends Equatable {
  const StationState();

  @override
  List<Object> get props => [];
}

class StationInitial extends StationState {}

class StationLoaded extends StationState {
  final StationPaginate stationPaginate;

  final double latitude;
  final double longitude;

  final List<ServiceFilter> serviceFilter;
  final List<FuelFilter> fuelFilter;

  final bool fetchMoreLoading;

  final int selected;

  bool get allowFetchMore =>
      !fetchMoreLoading && stationPaginate.page >= stationPaginate.current;

  Station? get selectedStation =>
      selected > -1 ? stationPaginate.stations[selected] : null;

  const StationLoaded(
      {required this.latitude,
      required this.longitude,
      required this.serviceFilter,
      required this.fuelFilter,
      required this.stationPaginate,
      this.fetchMoreLoading = false,
      this.selected = -1});

  @override
  List<Object> get props => [
        stationPaginate,
        allowFetchMore,
        fetchMoreLoading,
        fuelFilter,
        serviceFilter,
        selected,
      ];

  bool isSelected(Station station) {
    return selected > -1
        ? station.latitude == stationPaginate.stations[selected].latitude &&
            station.longitude == stationPaginate.stations[selected].longitude
        : false;
  }
}
