part of 'station_bloc.dart';

abstract class StationEvent extends Equatable {
  const StationEvent();

  @override
  List<Object> get props => [];
}

class RinitialiseStation extends StationEvent {
  const RinitialiseStation();

  @override
  List<Object> get props => [];
}

class LoadStation extends StationEvent {
  final List<ServiceFilter> serviceFilter;
  final List<FuelFilter> fuelFilter;

  const LoadStation({required this.serviceFilter, required this.fuelFilter});

  @override
  List<Object> get props => [serviceFilter, fuelFilter];
}

class FetchMoreStation extends StationEvent {}

class SelectStation extends StationEvent {
  final Station station;

  const SelectStation({required this.station});

  @override
  List<Object> get props => [station];
}

class UnSelectStation extends StationEvent {}
