import 'package:application/api/get_station_service.dart';
import 'package:application/models/fuel.dart';
import 'package:application/models/service.dart';
import 'package:application/models/station.dart';
import 'package:application/utils/location.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:graphql/client.dart';

part 'station_event.dart';
part 'station_state.dart';

class StationBloc extends Bloc<StationEvent, StationState> {
  final GraphQLClient client;

  StationBloc(this.client) : super(StationInitial()) {
    on<RinitialiseStation>((event, emit) => {emit(StationInitial())});
    on<LoadStation>((event, emit) async {
      Position userPosition = await getLocation();

      StationPaginate stations = await fetchStations(
        client: client,
        latitude: userPosition.latitude,
        longitude: userPosition.longitude,
        services: event.serviceFilter,
        fuels: event.fuelFilter,
      );

      emit(
        StationLoaded(
            latitude: userPosition.latitude,
            longitude: userPosition.longitude,
            serviceFilter: event.serviceFilter,
            fuelFilter: event.fuelFilter,
            stationPaginate: stations),
      );
    });

    on<FetchMoreStation>((event, emit) async {
      if (state is StationLoaded) {
        final state = this.state as StationLoaded;

        emit(StationLoaded(
            latitude: state.latitude,
            longitude: state.longitude,
            serviceFilter: state.serviceFilter,
            fuelFilter: state.fuelFilter,
            stationPaginate: state.stationPaginate,
            fetchMoreLoading: true));

        final StationPaginate moreStation = await fetchStations(
            client: client,
            latitude: state.latitude,
            longitude: state.longitude,
            services: state.serviceFilter,
            fuels: state.fuelFilter,
            page: state.stationPaginate.current + 1);

        state.stationPaginate.append(moreStation.stations, moreStation.current);

        emit(StationLoaded(
            latitude: state.latitude,
            longitude: state.longitude,
            serviceFilter: state.serviceFilter,
            fuelFilter: state.fuelFilter,
            stationPaginate: state.stationPaginate,
            fetchMoreLoading: false));
      }
    });

    on<SelectStation>((event, emit) {
      if (state is StationLoaded) {
        final state = this.state as StationLoaded;

        int index = state.stationPaginate.stations.indexWhere((element) =>
            element.latitude == event.station.latitude &&
            element.longitude == event.station.longitude);

        emit(StationLoaded(
          latitude: state.latitude,
          longitude: state.longitude,
          serviceFilter: state.serviceFilter,
          fuelFilter: state.fuelFilter,
          stationPaginate: state.stationPaginate,
          fetchMoreLoading: state.fetchMoreLoading,
          selected: index,
        ));
      }
    });

    on<UnSelectStation>((event, emit) {
      if (state is StationLoaded) {
        final state = this.state as StationLoaded;

        emit(StationLoaded(
          latitude: state.latitude,
          longitude: state.longitude,
          serviceFilter: state.serviceFilter,
          fuelFilter: state.fuelFilter,
          stationPaginate: state.stationPaginate,
          fetchMoreLoading: state.fetchMoreLoading,
          selected: -1,
        ));
      }
    });
  }
}
