part of 'fuel_filter_bloc.dart';

abstract class FuelFilterState extends Equatable {
  const FuelFilterState();

  @override
  List<Object> get props => [];
}

class FuelFilterInitial extends FuelFilterState {}

class FuelFilterLoaded extends FuelFilterState {
  final List<FuelFilter> allFuel;
  final List<FuelFilter> savedFuel;
  final List<FuelFilter> checkedFuel;

  const FuelFilterLoaded({
    required this.allFuel,
    this.savedFuel = const [],
    this.checkedFuel = const [],
  });

  @override
  List<Object> get props => [allFuel, savedFuel, checkedFuel];

  bool isChecked(FuelFilter service) {
    return checkedFuel.map((e) => e.id).contains(service.id);
  }

  bool compare() {
    final List<int> ref = savedFuel.map((e) => e.id).toList();
    final List<int> compare = checkedFuel.map((e) => e.id).toList();

    ref.sort((a, b) => a.compareTo(b));
    compare.sort((a, b) => a.compareTo(b));

    return !compare.equals(ref);
  }
}
