part of 'fuel_filter_bloc.dart';

abstract class FuelFilterEvent extends Equatable {
  const FuelFilterEvent();

  @override
  List<Object> get props => [];
}

class LoadFuelFilter extends FuelFilterEvent {}

class CheckFuelFilter extends FuelFilterEvent {
  final FuelFilter fuel;
  const CheckFuelFilter(this.fuel);

  @override
  List<Object> get props => [fuel];
}

class UnCheckFuelFilter extends FuelFilterEvent {
  final FuelFilter fuel;
  const UnCheckFuelFilter(this.fuel);

  @override
  List<Object> get props => [fuel];
}

class SaveCheckFuelFilter extends FuelFilterEvent {}
