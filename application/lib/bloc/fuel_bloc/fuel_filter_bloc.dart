import 'package:application/api/get_all_fuel.dart';
import 'package:application/bloc/fuel_bloc/fuel_filter_bloc.dart';
import 'package:application/models/fuel.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
export 'package:collection/collection.dart';

part 'fuel_filter_event.dart';
part 'fuel_filter_state.dart';

class FuelFilterBloc extends Bloc<FuelFilterEvent, FuelFilterState> {
  final GraphQLClient client;

  FuelFilterBloc({required this.client}) : super(FuelFilterInitial()) {
    on<LoadFuelFilter>((event, emit) async {
      final allFuel = await allFuelFilter(client);

      emit(FuelFilterLoaded(allFuel: allFuel));
    });

    on<CheckFuelFilter>((event, emit) {
      if (state is FuelFilterLoaded) {
        final state = this.state as FuelFilterLoaded;

        emit(FuelFilterLoaded(
          allFuel: state.allFuel,
          savedFuel: state.savedFuel,
          checkedFuel: [...state.checkedFuel, event.fuel],
        ));
      }
    });

    on<UnCheckFuelFilter>((event, emit) {
      if (state is FuelFilterLoaded) {
        final state = this.state as FuelFilterLoaded;

        emit(FuelFilterLoaded(
          allFuel: state.allFuel,
          savedFuel: state.savedFuel,
          checkedFuel: List.from(state.checkedFuel)..remove(event.fuel),
        ));
      }
    });

    on<SaveCheckFuelFilter>((event, emit) {
      if (state is FuelFilterLoaded) {
        final state = this.state as FuelFilterLoaded;

        emit(FuelFilterLoaded(
          allFuel: state.allFuel,
          savedFuel: [...state.checkedFuel],
          checkedFuel: state.checkedFuel,
        ));
      }
    });
  }
}
