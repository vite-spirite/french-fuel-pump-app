part of 'service_filter_bloc.dart';

abstract class ServiceFilterState extends Equatable {
  const ServiceFilterState();

  @override
  List<Object> get props => [];
}

class ServiceFilterInitial extends ServiceFilterState {}

class ServiceFilterLoaded extends ServiceFilterState {
  final List<ServiceFilter> allServices;
  final List<ServiceFilter> savedService;
  final List<ServiceFilter> checkService;

  const ServiceFilterLoaded(
      {required this.allServices,
      this.savedService = const [],
      this.checkService = const []});

  @override
  List<Object> get props => [allServices, savedService, checkService];

  bool isChecked(ServiceFilter service) {
    return checkService.map((e) => e.id).contains(service.id);
  }

  bool compare() {
    final List<int> ref = savedService.map((e) => e.id).toList();
    final List<int> compare = checkService.map((e) => e.id).toList();

    ref.sort((a, b) => a.compareTo(b));
    compare.sort((a, b) => a.compareTo(b));

    return !compare.equals(ref);
  }
}
