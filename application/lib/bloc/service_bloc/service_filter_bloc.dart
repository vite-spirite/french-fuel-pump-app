import 'package:application/api/get_all_services.dart';
import 'package:application/bloc/service_bloc/service_filter_bloc.dart';
import 'package:application/models/service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
export 'package:collection/collection.dart';

part 'service_filter_event.dart';
part 'service_filter_state.dart';

class ServiceFilterBloc extends Bloc<ServiceFilterEvent, ServiceFilterState> {
  final GraphQLClient client;

  ServiceFilterBloc({required this.client}) : super(ServiceFilterInitial()) {
    on<LoadServiceFilter>((event, emit) async {
      List<ServiceFilter> _services = await loadAllServiceFilter(client);
      emit(ServiceFilterLoaded(allServices: _services));
    });

    on<CheckServiceFilter>((event, emit) {
      if (state is ServiceFilterLoaded) {
        final state = this.state as ServiceFilterLoaded;

        emit(ServiceFilterLoaded(
          allServices: state.allServices,
          savedService: state.savedService,
          checkService: [...state.checkService, event.service],
        ));
      }
    });

    on<UnCheckServiceFilter>((event, emit) {
      if (state is ServiceFilterLoaded) {
        final state = this.state as ServiceFilterLoaded;

        emit(ServiceFilterLoaded(
          allServices: state.allServices,
          savedService: state.savedService,
          checkService: List.from(state.checkService)..remove(event.service),
        ));
      }
    });

    on<SaveCheckServiceFilter>((event, emit) {
      if (state is ServiceFilterLoaded) {
        final state = this.state as ServiceFilterLoaded;

        emit(
          ServiceFilterLoaded(
            allServices: state.allServices,
            savedService: state.checkService,
            checkService: state.checkService,
          ),
        );
      }
    });
  }
}
