part of 'service_filter_bloc.dart';

abstract class ServiceFilterEvent extends Equatable {
  const ServiceFilterEvent();

  @override
  List<Object> get props => [];
}

class LoadServiceFilter extends ServiceFilterEvent {}

class CheckServiceFilter extends ServiceFilterEvent {
  final ServiceFilter service;

  const CheckServiceFilter(this.service);

  @override
  List<Object> get props => [service];
}

class UnCheckServiceFilter extends ServiceFilterEvent {
  final ServiceFilter service;

  const UnCheckServiceFilter(this.service);

  @override
  List<Object> get props => [service];
}

class SaveCheckServiceFilter extends ServiceFilterEvent {
  const SaveCheckServiceFilter();

  @override
  List<Object> get props => [];
}
