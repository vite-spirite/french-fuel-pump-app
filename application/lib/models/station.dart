import 'package:latlong2/latlong.dart';

class StationPaginate {
  int page;
  int current;
  List<Station> stations;

  StationPaginate(
      {required this.page, required this.current, required this.stations});

  void append(List<Station> stations, int page) {
    this.stations = [...this.stations, ...stations];
    current = page;
  }
}

class Station {
  String address;

  double latitude;
  double longitude;
  double distance;

  List<StationService> services;
  List<StationFuel> fuels;
  List<StationOpening> open = [];

  String city;
  String cityCode;

  LatLng get point => LatLng(latitude, longitude);

  Station(
      {required this.address,
      required this.city,
      required this.cityCode,
      required this.latitude,
      required this.longitude,
      required this.distance,
      required this.services,
      required this.fuels,
      required List<StationOpening> opening})
      : super() {
    opening.sort((a, b) => a.dayOfWeek.compareTo(b.dayOfWeek));
    open = opening;
  }
}

class StationService {
  int id;
  String name;

  StationService({required this.id, required this.name});
}

class StationFuel {
  int id;
  String name;

  List<StationFuelPrice> _prices = [];

  List<StationFuelPrice> get prices => _prices;

  StationFuel(
      {required this.id,
      required this.name,
      required List<StationFuelPrice> prices})
      : super() {
    prices.sort((a, b) => b.update.compareTo(a.update));
    _prices = prices;
  }

  void addPrice(StationFuelPrice price) {
    _prices.add(price);
    prices.sort((a, b) => b.update.compareTo(a.update));
  }
}

class StationFuelPrice {
  double price;
  DateTime update;

  StationFuelPrice({required this.price, required this.update});
}

class StationOpening {
  int dayOfWeek;
  String open;
  String close;
  bool closure;

  StationOpening(
      {required this.dayOfWeek,
      required this.open,
      required this.close,
      required this.closure});
}
