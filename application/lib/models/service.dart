class ServiceFilter {
  final int _id;
  final String _name;

  int get id => _id;
  String get name => _name;

  ServiceFilter(this._id, this._name);
}
