import 'package:application/bloc/fuel_bloc/fuel_filter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_nord_theme/flutter_nord_theme.dart';

import '../models/fuel.dart';
import '../widgets/filter_button.dart';

class FuelFilterScreen extends StatelessWidget {
  const FuelFilterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FuelFilterBloc, FuelFilterState>(
      builder: (context, state) {
        if (state is FuelFilterLoaded) {
          return buildFilterList(context, "Essences:");
        } else {
          return const Text("Une erreur est survenue.");
        }
      },
    );
  }

  void onSavedPressed(BuildContext context) {
    context.read<FuelFilterBloc>().add(SaveCheckFuelFilter());
    Navigator.pop(context);
  }

  void onSelectFilter(BuildContext context, FuelFilter fuel) {
    context.read<FuelFilterBloc>().add(CheckFuelFilter(fuel));
  }

  void onUnSelectFilter(BuildContext context, FuelFilter fuel) {
    context.read<FuelFilterBloc>().add(UnCheckFuelFilter(fuel));
  }

  Widget buildFilterList(BuildContext context, String title) {
    FuelFilterLoaded provider =
        (BlocProvider.of<FuelFilterBloc>(context).state as FuelFilterLoaded);

    List<FuelFilter> services = provider.allFuel;
    List<Widget> filters = [];

    for (var item in services) {
      filters.add(
        BlocSelector<FuelFilterBloc, FuelFilterState, bool>(
          selector: (state) =>
              (state is FuelFilterLoaded && state.isChecked(item)
                  ? true
                  : false),
          builder: (ctx, selected) => FilterButton(
            text: item.name,
            selected: selected,
            onSelect: () => onSelectFilter(ctx, item),
            onUnSelect: () => onUnSelectFilter(ctx, item),
          ),
        ),
      );
    }

    return SingleChildScrollView(
      child: Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 32),
                child: Text(
                  title,
                  style: TextStyle(
                      color: MediaQuery.of(context).platformBrightness ==
                              Brightness.dark
                          ? NordColors.$5
                          : NordColors.$0,
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
              ),
              Wrap(
                spacing: 8.0,
                children: <Widget>[...filters],
              ),
            ]),
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(16, 32, 16, 32),
      ),
    );
  }
}
