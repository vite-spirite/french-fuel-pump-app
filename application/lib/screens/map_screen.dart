import 'package:application/bloc/fuel_bloc/fuel_filter_bloc.dart';
import 'package:application/bloc/service_bloc/service_filter_bloc.dart';
import 'package:application/bloc/station_bloc/station_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_nord_theme/flutter_nord_theme.dart';
import 'package:latlong2/latlong.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../models/station.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  double panelClosedHeight = 150.0;
  double panelOpenHeight = 500.0;
  double panelHeight = 0.0;

  bool panelOpened = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocBuilder<StationBloc, StationState>(
            builder: (context, stationBloc) {
          if (stationBloc is StationInitial) {
            final service = context.read<ServiceFilterBloc>().state;
            final fuel = context.read<FuelFilterBloc>().state;

            context.read<StationBloc>().add(LoadStation(
                serviceFilter: (service is ServiceFilterLoaded)
                    ? service.savedService
                    : [],
                fuelFilter: (fuel is FuelFilterLoaded) ? fuel.savedFuel : []));
            return const CircularProgressIndicator();
          } else if (stationBloc is StationLoaded) {
            return Stack(children: [
              _buildMap(context),
              BlocSelector<StationBloc, StationState, Station?>(
                selector: (state) =>
                    state is StationLoaded && state.selectedStation != null
                        ? state.selectedStation
                        : null,
                builder: (context, station) => station == null
                    ? Container()
                    : _buildStationInformation(context, station),
              ),
              Positioned(
                height: (panelOpened
                    ? MediaQuery.of(context).size.height -
                        panelHeight -
                        panelClosedHeight -
                        10.0
                    : MediaQuery.of(context).size.height - 10.0),
                right: 10.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    _buildFetchMoreButton(context),
                    FloatingActionButton(
                      heroTag: 2,
                      onPressed: () {
                        Navigator.pushNamed(context, '/filters');
                      },
                      child: const Icon(
                        Icons.filter_list,
                        color: NordColors.$0,
                      ),
                    ),
                  ],
                ),
              ),
            ]);
          } else {
            return const Text('Une erreur est survenue !');
          }
        }),
      ),
    );
  }

  Widget _buildFetchMoreButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
      child: BlocSelector<StationBloc, StationState, bool>(
        selector: (state) => state is StationLoaded && state.allowFetchMore,
        builder: (context, active) => FloatingActionButton(
          heroTag: 1,
          onPressed: active
              ? () {
                  context.read<StationBloc>().add(FetchMoreStation());
                }
              : null,
          child: BlocSelector<StationBloc, StationState, bool>(
            selector: (state) =>
                state is StationLoaded && state.fetchMoreLoading,
            builder: (context, isLoading) => isLoading
                ? const CircularProgressIndicator(
                    strokeWidth: 2.0,
                  )
                : const Icon(
                    Icons.refresh,
                    color: NordColors.$0,
                  ),
          ),
          backgroundColor: active ? NordColors.aurora.green : NordColors.$1,
        ),
      ),
    );
  }

  Widget _buildMap(BuildContext context) {
    final station = context.read<StationBloc>().state as StationLoaded;

    return SizedBox(
      child: FlutterMap(
        options: MapOptions(
            center: LatLng(station.latitude, station.longitude), zoom: 13.0),
        layers: [
          TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c'],
            attributionBuilder: (_) {
              return const Text("© OpenStreetMap");
            },
          ),
          MarkerLayerOptions(
            markers: [
              Marker(
                point: LatLng(station.latitude, station.longitude),
                builder: (ctx) => Icon(
                  Icons.person_pin_circle,
                  color: NordColors.aurora.orange,
                  size: 40.0,
                ),
                width: 80.0,
                height: 80.0,
              ),
              ...station.stationPaginate.stations.map(
                (e) => Marker(
                  point: e.point,
                  width: 80.0,
                  height: 80.0,
                  builder: (ctx) => GestureDetector(
                    child: BlocSelector<StationBloc, StationState, bool>(
                      selector: (state) =>
                          state is StationLoaded && state.isSelected(e),
                      builder: (ctx, selected) => Icon(
                        Icons.circle,
                        color: selected
                            ? NordColors.frost.lightest
                            : NordColors.frost.lighter,
                        size: selected ? 40.0 : 20.0,
                      ),
                    ),
                    onTap: () {
                      context
                          .read<StationBloc>()
                          .add(SelectStation(station: e));

                      panelOpened = true;
                    },
                  ),
                ),
              ),
            ],
          )
        ],
      ),
      height: MediaQuery.of(context).size.height - panelHeight,
    );
  }

  Widget _buildStationInformation(BuildContext context, Station station) {
    panelOpenHeight = MediaQuery.of(context).size.height * .60;
    panelOpened = true;

    return SlidingUpPanel(
      minHeight: panelClosedHeight,
      maxHeight: panelOpenHeight,
      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
      panel: SingleChildScrollView(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  textDirection: TextDirection.ltr,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 100.0,
                      child: Text(
                        station.address.toUpperCase(),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: const TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: NordColors.$4),
                      ),
                    ),
                    Text(
                      '${station.cityCode} ${station.city.toUpperCase()}',
                      style: const TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: NordColors.$4),
                    ),
                  ],
                ),
                IconButton(
                  onPressed: () {
                    context.read<StationBloc>().add(UnSelectStation());

                    setState(() {
                      panelHeight = 0.0;
                      panelOpened = false;
                    });
                  },
                  icon: Icon(
                    Icons.close,
                    color: NordColors.aurora.red,
                  ),
                ),
              ],
            ),
          ),
          const Divider(height: 2.0),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: const [
                    Text(
                      "Services:",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: NordColors.$4,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      ...station.services.map((e) => _buildChip(e.name)),
                    ],
                  ),
                )
              ],
            ),
          ),
          const Divider(height: 2.0),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: const [
                    Text(
                      "Essences:",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: NordColors.$4,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: _buildPriceTable(station.fuels),
                  ),
                )
              ],
            ),
          ),
        ]),
      ),
      color: NordColors.$0,
      onPanelSlide: (height) {
        setState(() {
          panelHeight = height * (panelOpenHeight - panelClosedHeight);
        });
      },
    );
  }

  Widget _buildChip(String name) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(2.0, 1.0, 2.0, 1.0),
      child: Chip(label: Text(name)),
    );
  }

  List<Widget> _buildPriceTable(List<StationFuel> fuel) {
    List<Widget> table = [];

    for (var item in fuel) {
      table.add(_buildPrice(item));
      table.add(const Divider(height: 2.0));
    }

    table.removeLast();
    return table;
  }

  Widget _buildPrice(StationFuel fuel) {
    print(fuel.prices.map((e) => '${e.update.day}/${e.update.month}'));

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Text(
            fuel.name.toUpperCase(),
            style: const TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: NordColors.$4,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '${fuel.prices[0].price}€ (${fuel.prices[0].update.day}/${fuel.prices[0].update.month})',
                style: TextStyle(
                  color: fuel.prices.length > 2 &&
                          fuel.prices[0].price > fuel.prices[1].price
                      ? NordColors.aurora.red
                      : NordColors.aurora.green,
                ),
              ),
              Icon(
                fuel.prices.length > 2 &&
                        fuel.prices[0].price > fuel.prices[1].price
                    ? Icons.keyboard_arrow_up
                    : Icons.keyboard_arrow_down,
                color: fuel.prices.length > 2 &&
                        fuel.prices[0].price > fuel.prices[1].price
                    ? NordColors.aurora.red
                    : NordColors.aurora.green,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
