import 'package:application/bloc/fuel_bloc/fuel_filter_bloc.dart';
import 'package:application/bloc/service_bloc/service_filter_bloc.dart';
import 'package:application/bloc/station_bloc/station_bloc.dart';
import 'package:application/screens/filter_fuel_screen.dart';
import 'package:application/screens/filter_service_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_nord_theme/flutter_nord_theme.dart';

class FilterScreen extends StatelessWidget {
  const FilterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Filtres'),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        foregroundColor:
            MediaQuery.of(context).platformBrightness == Brightness.dark
                ? NordColors.$5
                : NordColors.$0,
        bottom: const PreferredSize(
          child: Divider(height: 2.0),
          preferredSize: Size.fromHeight(2.0),
        ),
      ),
      floatingActionButton:
          BlocSelector<ServiceFilterBloc, ServiceFilterState, bool>(
        selector: (state) =>
            state is ServiceFilterLoaded && state.compare() ? true : false,
        builder: (context, service) =>
            BlocSelector<FuelFilterBloc, FuelFilterState, bool>(
          selector: (state) =>
              state is FuelFilterLoaded && state.compare() ? true : false,
          builder: (_, fuel) => FloatingActionButton(
            onPressed: service || fuel ? () => onSavedPressed(context) : null,
            child: Icon(
              Icons.check,
              color: service || fuel ? NordColors.$1 : NordColors.$2,
            ),
            backgroundColor:
                service || fuel ? NordColors.aurora.green : NordColors.$1,
            elevation: 15.0,
            disabledElevation: 0,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Center(
      child: BlocBuilder<ServiceFilterBloc, ServiceFilterState>(
        builder: (context, serviceFilterState) =>
            BlocBuilder<FuelFilterBloc, FuelFilterState>(
          builder: (_, fuelFilterState) {
            if (serviceFilterState is ServiceFilterInitial ||
                fuelFilterState is FuelFilterInitial) {
              return const CircularProgressIndicator();
            }

            if (serviceFilterState is ServiceFilterLoaded &&
                fuelFilterState is FuelFilterLoaded) {
              return SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    FuelFilterScreen(),
                    Divider(height: 2.0),
                    FilterServiceScreen()
                  ],
                ),
              );
            }

            return const Text("Une erreur est survenue.");
          },
        ),
      ),
    );
  }

  void onSavedPressed(BuildContext context) {
    context.read<ServiceFilterBloc>().add(const SaveCheckServiceFilter());
    context.read<FuelFilterBloc>().add(SaveCheckFuelFilter());

    final service =
        context.read<ServiceFilterBloc>().state as ServiceFilterLoaded;
    final fuel = context.read<FuelFilterBloc>().state as FuelFilterLoaded;

    context.read<StationBloc>().add(RinitialiseStation());
    Navigator.pop(context);
  }
}
