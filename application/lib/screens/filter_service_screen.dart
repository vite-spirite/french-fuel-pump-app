import 'package:application/bloc/service_bloc/service_filter_bloc.dart';
import 'package:application/models/service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_nord_theme/flutter_nord_theme.dart';

import '../widgets/filter_button.dart';

class FilterServiceScreen extends StatelessWidget {
  const FilterServiceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ServiceFilterBloc, ServiceFilterState>(
      builder: (context, state) {
        if (state is ServiceFilterLoaded) {
          return SingleChildScrollView(
            child: buildFilterList(context, "Services:"),
          );
        } else {
          return const Text("Une erreur est survenue.");
        }
      },
    );
  }

  void onSavedPressed(BuildContext context) {
    context.read<ServiceFilterBloc>().add(const SaveCheckServiceFilter());
    Navigator.pop(context);
  }

  void onSelectFilter(BuildContext context, ServiceFilter service) {
    context.read<ServiceFilterBloc>().add(CheckServiceFilter(service));
  }

  void onUnSelectFilter(BuildContext context, ServiceFilter service) {
    context.read<ServiceFilterBloc>().add(UnCheckServiceFilter(service));
  }

  Widget buildFilterList(BuildContext context, String title) {
    ServiceFilterLoaded provider = (BlocProvider.of<ServiceFilterBloc>(context)
        .state as ServiceFilterLoaded);

    List<ServiceFilter> services = provider.allServices;
    List<Widget> filters = [];

    for (var item in services) {
      filters.add(
        BlocSelector<ServiceFilterBloc, ServiceFilterState, bool>(
          selector: (state) =>
              (state is ServiceFilterLoaded && state.isChecked(item)
                  ? true
                  : false),
          builder: (ctx, selected) => FilterButton(
            text: item.name,
            selected: selected,
            onSelect: () => onSelectFilter(ctx, item),
            onUnSelect: () => onUnSelectFilter(ctx, item),
          ),
        ),
      );
    }

    return SingleChildScrollView(
      child: Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 32),
                child: Text(
                  title,
                  style: TextStyle(
                      color: MediaQuery.of(context).platformBrightness ==
                              Brightness.dark
                          ? NordColors.$5
                          : NordColors.$0,
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
              ),
              Wrap(
                spacing: 8.0,
                children: <Widget>[...filters],
              ),
            ]),
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.fromLTRB(16, 32, 16, 32),
      ),
    );
  }
}
