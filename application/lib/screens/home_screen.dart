import 'package:application/screens/filter_screen.dart';
import 'package:application/screens/in_build_screen.dart';
import 'package:application/screens/map_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_nord_theme/flutter_nord_theme.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: NordColors.$0,
        systemNavigationBarDividerColor: NordColors.$0,
      ),
    );

    return MaterialApp(
      title: 'Flutter Demo',
      theme: NordTheme.light(),
      darkTheme: NordTheme.dark(),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      routes: {
        "/": (context) => const MapScreen(),
        "/filters": (context) => const FilterScreen(),
      },
      initialRoute: "/",
    );
  }
}
