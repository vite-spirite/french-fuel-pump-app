import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';

Future<Position> getLocation() async {
  bool enabled = false;
  LocationPermission permission;

  enabled = await Geolocator.isLocationServiceEnabled();

  if (!enabled) {
    await Geolocator.openLocationSettings();
    return Future.error('location permission denied !');
  }

  permission = await Geolocator.checkPermission();

  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error('location permission denied !');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    return Future.error('location permission denied !');
  }

  if (kDebugMode) {
    print("permitted");
  }

  return await Geolocator.getCurrentPosition();
}
