import 'package:application/bloc/fuel_bloc/fuel_filter_bloc.dart';
import 'package:application/bloc/service_bloc/service_filter_bloc.dart';
import 'package:application/bloc/station_bloc/station_bloc.dart';
import 'package:application/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    GraphQLClient client = getClient();

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              ServiceFilterBloc(client: client)..add(LoadServiceFilter()),
        ),
        BlocProvider(
          create: (context) =>
              FuelFilterBloc(client: client)..add(LoadFuelFilter()),
        ),
        BlocProvider(
            create: (context) =>
                StationBloc(client)..add(const RinitialiseStation())),
      ],
      child: const HomePage(),
    );
  }

  GraphQLClient getClient() {
    HttpLink link = HttpLink("http://192.168.1.2:3000/graphql");

    return GraphQLClient(link: link, cache: GraphQLCache());
  }
}
