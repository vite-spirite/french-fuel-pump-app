import 'package:application/models/fuel.dart';
import 'package:graphql/client.dart';

String getFuelFilter = '''
query getFilters
{
  getFuelType {
    id
    name
  }
}
''';

Future<List<FuelFilter>> allFuelFilter(GraphQLClient client) async {
  final result = await client.query(QueryOptions(document: gql(getFuelFilter)));

  List<dynamic> rawFuels = result.data?['getFuelType'];
  List<FuelFilter> apiFuels =
      rawFuels.map((e) => FuelFilter(int.parse(e['id']), e['name'])).toList();

  return apiFuels;
}
