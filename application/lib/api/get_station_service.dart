import 'package:application/models/station.dart';
import 'package:graphql/client.dart';

import '../models/fuel.dart';
import '../models/service.dart';

String getStationService = '''
query getStations(\$coords: CoordUserType!, \$filters: StationFilterInput, \$page: Float) {
  getStations(coords: \$coords, filters: \$filters, page: \$page) {
    page
    current
    stations {
      address
      lat
      lng
      distance
      services {
        id
        name
      }
      city {
        code
        name
      }
      prices {
        fuel {
          id
          name
        }
        price
        update
      }
      OpeningTimes {
        close
        closure
        open
        dayOfWeek
      }
    }
  }
}
''';

Future<StationPaginate> fetchStations({
  required GraphQLClient client,
  required double latitude,
  required double longitude,
  int page = 0,
  List<ServiceFilter> services = const [],
  List<FuelFilter> fuels = const [],
}) async {
  QueryResult result = await client.query(
    QueryOptions(document: gql(getStationService), variables: {
      'coords': {
        'lat': latitude,
        'lng': longitude,
      },
      'filters': {
        'services': services.map((e) => e.id).toList(),
        'fuels': fuels.map((e) => e.id).toList()
      },
      'page': page
    }),
  );

  List<dynamic> raw = result.data?['getStations']?['stations'];
  List<Station> stations = [];

  for (var station in raw) {
    List<StationFuel> fuels = [];

    for (var item in station['prices']) {
      int id = item['fuel']['id'] is int
          ? item['fuel']['id']
          : int.parse(item['fuel']['id']);

      int index = fuels.indexWhere((element) => element.id == id);

      if (index == -1) {
        fuels.add(StationFuel(id: id, name: item['fuel']['name'], prices: []));
        index = fuels.indexWhere((element) => element.id == id);
      }

      if (index != -1) {
        fuels[index].addPrice(
          StationFuelPrice(
            price: item['price'] / 1000,
            update: DateTime.parse(item['update']),
          ),
        );
      }
    }

    List<StationService> services = (station['services'] as List<dynamic>)
        .map(
          (e) => StationService(
              id: e['id'] is int ? e['id'] : int.parse(e['id']),
              name: e['name']),
        )
        .toList();

    stations.add(
      Station(
          address: station['address'],
          city: station['city']['name'],
          cityCode: station['city']['code'],
          latitude: station['lat'] is double
              ? station['lat']
              : double.parse(station['lat']),
          longitude: station['lng'] is double
              ? station['lng']
              : double.parse(station['lng']),
          distance: station['distance'] is double
              ? station['distance']
              : double.parse(station['distance']),
          services: services,
          fuels: fuels,
          opening: []),
    );
  }

  return StationPaginate(
      page: result.data?['getStations']?['page'] is int
          ? result.data?['getStations']?['page']
          : int.parse(result.data?['getStations']?['page']),
      current: result.data?['getStations']?['current'] is int
          ? result.data?['getStations']?['current']
          : int.parse(result.data?['getStations']?['current']),
      stations: stations);
}
