import 'package:application/models/service.dart';
import 'package:graphql/client.dart';

String getServicesQuery = '''
query getFilters
{
  getServices
  {
    name
    id
  }
}
''';

/*

  */

Future<List<ServiceFilter>> loadAllServiceFilter(GraphQLClient client) async {
  QueryResult result =
      await client.query(QueryOptions(document: gql(getServicesQuery)));
  List<dynamic> rawServices = result.data?['getServices'];
  List<ServiceFilter> apiServices = rawServices
      .map(
          (element) => ServiceFilter(int.parse(element['id']), element['name']))
      .toList();

  apiServices.sort((a, b) => a.name.length.compareTo(b.name.length));

  return apiServices;
}
