import 'package:flutter/material.dart';
import 'package:flutter_nord_theme/flutter_nord_theme.dart';

class FilterButton extends StatelessWidget {
  const FilterButton(
      {Key? key,
      required this.text,
      required this.onSelect,
      required this.onUnSelect,
      required this.selected})
      : super(key: key);

  final String text;
  final void Function() onSelect;
  final void Function() onUnSelect;

  final bool selected;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: selected ? onUnSelect : onSelect,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith(
          (states) {
            if (MediaQuery.of(context).platformBrightness == Brightness.dark) {
              return selected ? NordColors.frost.lighter : NordColors.$1;
            } else {
              return selected
                  ? NordColors.frost.lighter
                  : NordColors.snowStorm.lightest;
            }
          },
        ),
        side: MaterialStateProperty.resolveWith((states) {
          if (MediaQuery.of(context).platformBrightness == Brightness.dark) {
            return BorderSide(
              color: selected
                  ? NordColors.frost.lighter
                  : NordColors.snowStorm.medium,
            );
          } else {
            return BorderSide(
              color: selected ? NordColors.frost.lighter : NordColors.$1,
            );
          }
        }),
        shape: MaterialStateProperty.resolveWith(
          (states) => RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
      ),
      child: Text(
        text,
        style: MediaQuery.of(context).platformBrightness == Brightness.dark
            ? TextStyle(
                color: selected
                    ? NordColors.polarNight.darkest
                    : NordColors.snowStorm.medium,
              )
            : TextStyle(
                color: selected
                    ? NordColors.snowStorm.medium
                    : NordColors.polarNight.darkest,
              ),
      ),
    );
  }
}


/*
=> BorderSide(
            color: selected
                ? NordColors.frost.lighter
                : NordColors.snowStorm.medium)
                */