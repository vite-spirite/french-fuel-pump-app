echo "fetch gouvernement data"
curl "https://donnees.roulez-eco.fr/opendata/instantane" | zcat > fuel.xml
echo "generate csv file"
ruby xml_to_csv.rb
echo "run postgres script"
docker exec -it fuel_pump_db psql developer -d fuelpump -f /dump/fuelpump.sql
echo "finish dump database"