CREATE TABLE IF NOT EXISTS temp_stations (
  "gouv_id" INTEGER PRIMARY KEY UNIQUE,
  "lat" FLOAT,
  "lng" FLOAT,
  "city_name" VARCHAR,
  "city_code" VARCHAR,
  "address" VARCHAR,
  "services" VARCHAR,
  "automaton" INTEGER,
  "opening" VARCHAR,
  "prices" VARCHAR
);

CREATE TABLE IF NOT EXISTS temp_services (
  id SERIAL PRIMARY KEY,
  name VARCHAR,
  gouv_id INTEGER
);

CREATE TABLE IF NOT EXISTS temp_schedules (
  id SERIAL PRIMARY KEY,
  open_string TEXT,
  "stationId" INTEGER
);

CREATE TABLE IF NOT EXISTS temp_prices (
  id SERIAL PRIMARY KEY,
  station_gouv_id INTEGER,
  fuel_price_string VARCHAR
);

TRUNCATE TABLE temp_services RESTART IDENTITY;
TRUNCATE TABLE temp_stations RESTART IDENTITY;
TRUNCATE TABLE temp_schedules RESTART IDENTITY;
TRUNCATE TABLE temp_prices RESTART IDENTITY;

\copy temp_stations FROM '/dump/fuel.csv' DELIMITER ',' HEADER CSV;

INSERT INTO cities ("name", "code")
  (SELECT DISTINCT ON (ts."city_code") "city_name", "city_code" FROM "temp_stations" AS ts WHERE NOT EXISTS 
  (SELECT code FROM "cities" AS c WHERE ts.city_code = c.code));

INSERT INTO "temp_services" (name, gouv_id) (SELECT unnest(string_to_array(services, ';')) as name, gouv_id FROM "temp_stations");
INSERT INTO "services" (name) (SELECT DISTINCT ON (ts.name) name FROM "temp_services" as ts WHERE NOT EXISTS (SELECT name FROM "services" as s WHERE ts.name = s.name));

UPDATE "stations" SET
      lat = ts.lat,
      lng = ts.lng,
      address = ts.address,
      automaton = ts.automaton,
      "cityId" = ts."cityId" FROM (
        SELECT gouv_id, lat, lng, address, (automaton)::int::bool, (SELECT cities.id FROM cities WHERE cities.code = city_code) AS "cityId" FROM "temp_stations"
      ) ts WHERE ts.gouv_id = stations.gouv_id;

INSERT INTO stations (gouv_id, lat, lng, address, automaton, "cityId") 
  (SELECT ts.gouv_id, ts.lat, ts.lng, ts.address, (ts.automaton)::int::bool, (SELECT cities.id FROM cities WHERE cities.code = ts.city_code) AS "cityId" FROM "temp_stations" as ts WHERE NOT EXISTS (SELECT gouv_id FROM "stations" AS s WHERE s.gouv_id = ts.gouv_id));

INSERT INTO stations_services_services ("stationsId", "servicesId")
  (SELECT (SELECT s.id FROM stations AS s WHERE s.gouv_id = ts.gouv_id) as "stationsId", (SELECT id FROM services AS srv WHERE srv.name = ts.name) as "servicesId" FROM temp_services as ts WHERE NOT EXISTS (SELECT s."stationsId", s."servicesId" FROM stations_services_services AS s WHERE s."stationsId" = "stationsId" AND s."servicesId" = "servicesId"));

INSERT INTO temp_schedules (open_string, "stationId")
  (SELECT unnest(string_to_array(opening, ';')), (SELECT id FROM stations WHERE stations.gouv_id = temp_stations.gouv_id) as "stationId" FROM temp_stations);

INSERT INTO schedules ("stationId", "dayOfWeek", "closure", "open", "close")
  (SELECT "stationId", (arr[1])::int as "dayOfWeek", (arr[2])::int::bool as "closure", arr[3] as open, arr[4] as close FROM temp_schedules, string_to_array(temp_schedules.open_string, '|') as arr WHERE NOT EXISTS (SELECT id FROM schedules WHERE temp_schedules."stationId" = schedules."stationId" AND (arr[1])::int = schedules."dayOfWeek"));

UPDATE schedules SET 
  "closure" = ref."closure",
  "open" = ref."open",
  "close" = ref."close" FROM
    (SELECT "stationId", (arr[1])::int as "dayOfWeek", (arr[2])::int::bool as "closure", arr[3] as open, arr[4] as close FROM temp_schedules, string_to_array(temp_schedules.open_string, '|') as arr) as ref WHERE ref."dayOfWeek" = schedules."dayOfWeek" AND ref."stationId" = schedules."stationId";

INSERT INTO temp_prices ("station_gouv_id", "fuel_price_string")
  (SELECT gouv_id as station_gouv_id, unnest(string_to_array(prices, ';')) as fuel_price_string FROM "temp_stations");

INSERT INTO "fuel_type" (gouv_id, name)
  (SELECT DISTINCT ON(gouv_id) (arr[1])::int AS gouv_id, arr[2] AS name FROM "temp_prices" as tp, string_to_array(fuel_price_string, '|') as arr WHERE NOT EXISTS 
    (SELECT ft.id FROM "fuel_type" as ft WHERE ft.gouv_id = (arr[1])::int));
  
INSERT INTO "station_fuel_prices" (is_distribute, price, update, "fuelId", "stationId")
  (SELECT arr[3]::int::bool, ((arr[4]::float * 1000)::int), TO_TIMESTAMP(arr[5], 'YYYY-MM-DD HH24:MI:SS'), (SELECT id FROM "fuel_type" as ft WHERE ft.gouv_id = arr[1]::int) as fuel, (SELECT id FROM "stations" as s WHERE s.gouv_id = tp.station_gouv_id) as station FROM "temp_prices" as tp, string_to_array(fuel_price_string, '|') as arr WHERE NOT EXISTS 
  (SELECT id FROM "station_fuel_prices" as sfp WHERE sfp."fuelId" = (SELECT id FROM "fuel_type" as ft WHERE ft.gouv_id = arr[1]::int) AND sfp."stationId" = (SELECT id FROM "stations" as s WHERE s.gouv_id = tp.station_gouv_id) AND sfp.update = TO_TIMESTAMP(arr[5], 'YYYY-MM-DD HH24:MI:SS')));

DROP TABLE "temp_prices", "temp_schedules", "temp_services", "temp_stations";