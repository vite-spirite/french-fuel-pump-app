#! /usr/bin/env ruby
require 'csv'
require 'nokogiri'

csv = ["gouv_id", "lat", "lng", "city_name", "city_code", "address", "services", "automaton", "opening", "prices"].to_csv;

doc = Nokogiri::XML(File.open("fuel.xml"))
doc.search('pdv').each do |pdv|
    row = [
        pdv["id"].to_i,
        pdv["latitude"].to_f/100000,
        pdv["longitude"].to_f/100000,
        pdv.search("ville").text.downcase || nil,
        pdv['cp'],
        pdv.search("adresse").text.downcase || nil
    ]

    services = [];
    pdv.search("service").each do |srv|
        services.append(srv.text);
    end

    row.append(services.join(';'));

    days = [];
    openingXML = pdv.search("horaires") rescue nil;
    if(openingXML)

        automaton = openingXML.attribute("automate-24-24") || "0"

        if automaton.to_s.length == 0
            automaton = "0"
        end
        
        row.append(automaton);

        openingXML.search("jour").each do |day|
            time = day.search("horaire");
            openure = time.attribute("ouverture") || ""
            closure = time.attribute("fermeture") || ""

            isFerme = "0";

            if day['ferme'] == "1"
                isFerme = "1"
            end

            timing = [
                day["id"].to_i,
                isFerme 
            ]

            timing.append(openure);
            timing.append(closure);
            days.append(timing.join('|'))

        end
    end
    row.append(days.join(';'));

    prices = [];


    pdv.search('prix').each do |price|
        prices.append([
            price['id'],
            price['nom'],
            "1",
            price['valeur'],
            price['maj']
        ].join('|'));
    end

    pdv.search('rupture').each do |price|
        prices.apprend([
            price['id'],
            price['nom'],
            "0",
            "0000",
            price['debut']
        ].join("|"));
    end

    row.append(prices.join(';'));

    csv << row.to_csv
end

File.write("container_files/fuel.csv", csv, mode: "w");