CREATE DATABASE fuelpump;
\c fuelpump;

CREATE OR REPLACE FUNCTION getDistanceStation(lat1 float, lng1 float, lat2 float, lng2 float) 
    RETURNS float AS $BODY$

DECLARE
    r int;
    lat1radians float;
    lat2radians float;
    latdelta float;
    lngdelta float;
    a float;
    b float;
    c float;
BEGIN
    SELECT 6371 INTO r;
    SELECT radians(lat1) INTO lat1radians;
    SELECT radians(lat2) INTO lat2radians;
    SELECT radians(lat2 - lat1) INTO latdelta;
    SELECT radians(lng2 - lng1) INTO lngdelta;

    SELECT sin(latdelta/2) * sin(latdelta/2) + cos(lat1radians) * cos(lat2radians) * (sin(lngdelta/2) * sin(lngdelta/2)) INTO a;
    SELECT 2 * atan2(sqrt(a), sqrt(1-a)) INTO b;
    SELECT r * b INTO c;
    return c;
END;
$BODY$ LANGUAGE plpgsql;